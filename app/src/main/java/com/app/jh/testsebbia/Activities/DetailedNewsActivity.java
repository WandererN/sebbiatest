package com.app.jh.testsebbia.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.jh.testsebbia.DataSources.IDataSource;
import com.app.jh.testsebbia.DataSources.RestDataSource;
import com.app.jh.testsebbia.DataStructures.FullNewsItem;
import com.app.jh.testsebbia.DataStructures.NewsItem;
import com.app.jh.testsebbia.R;

import java.util.concurrent.CancellationException;

public class DetailedNewsActivity extends AppCompatActivity {
    private NewsItem newsItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.idDetailedNewsToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.app_name);
        getSupportActionBar().setSubtitle(R.string.details);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createFullNews();
            }
        });
        Intent intent = getIntent();
        newsItem = new NewsItem(
                intent.getLongExtra("newsId", 0),
                intent.getStringExtra("title"),
                intent.getStringExtra("data"),
                intent.getStringExtra("shortDescr")
        );
        ((TextView) findViewById(R.id.idNewsTitle)).setText(newsItem.getTitle());
        ((TextView) findViewById(R.id.idNewsData)).setText(newsItem.getDate());
        ((TextView) findViewById(R.id.idNewsShortDescr)).setText(newsItem.getShortDescription());

        createFullNews();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void createFullNews() {
        try {
            IDataSource dataSource = new RestDataSource(getApplicationContext());

            FullNewsItem fullNewsItem = dataSource.getDetailedNewsInfo(newsItem.getId());
            ((TextView) findViewById(R.id.idNewsTitle)).setText(fullNewsItem.getTitle());
            ((TextView) findViewById(R.id.idNewsData)).setText(fullNewsItem.getDate());
            ((TextView) findViewById(R.id.idNewsShortDescr)).setText(fullNewsItem.getShortDescription());
            TextView fullDescTextView = (TextView) findViewById(R.id.idNewsFullDescr);
            fullDescTextView.setText(Html.fromHtml(fullNewsItem.getFullDescription()));
            fullDescTextView.setMovementMethod(LinkMovementMethod.getInstance());
            Snackbar.make(findViewById(R.id.content_detailed_news), getString(R.string.one_news_loaded), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        } catch (CancellationException e) {
            Snackbar.make(findViewById(R.id.content_detailed_news), getString(R.string.no_connection), Snackbar.LENGTH_LONG).
                    setAction("Action", null).show();
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
