package com.app.jh.testsebbia.ListImprovers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.jh.testsebbia.DataStructures.NewsItem;
import com.app.jh.testsebbia.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Странник on 17.03.2017.
 */

public class NewsItemListAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private final List<NewsItem> items = new ArrayList<>();

    public NewsItemListAdapter(LayoutInflater inflater, List<NewsItem> items) {
        this.inflater = inflater;
        this.items.addAll(items);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public NewsItem getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View retView = view;
        if (view == null) {
            retView = inflater.inflate(R.layout.category_list_item, viewGroup, false);
        }

        NewsItem item = getItem(i);
        ((TextView) retView.findViewById(R.id.idDateCaption)).setText(item.getDate());
        ((TextView) retView.findViewById(R.id.idShortDescr)).setText(item.getShortDescription());
        ((TextView) retView.findViewById(R.id.idTitle)).setText(item.getTitle());

        return retView;
    }

    public void addList(List<NewsItem> addItems) {
        items.addAll(addItems);
        notifyDataSetChanged();
    }
}
