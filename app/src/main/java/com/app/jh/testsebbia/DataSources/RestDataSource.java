package com.app.jh.testsebbia.DataSources;

import android.content.Context;
import android.net.Uri;
import android.webkit.URLUtil;

import com.app.jh.testsebbia.DataStructures.CategoryItem;
import com.app.jh.testsebbia.DataStructures.FullNewsItem;
import com.app.jh.testsebbia.DataStructures.NewsItem;
import com.app.jh.testsebbia.R;
import com.app.jh.testsebbia.Utils.RestHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

/**
 * Created by Странник on 15.03.2017.
 */

public class RestDataSource implements IDataSource {
    private final Context context;
    //Server root:
    private final String SERVER_URL; //= "http://testtask.sebbia.com";
    //API addresses:
    private final String CATEGORIES;// = "/v1/news/categories";
    private final String NEWS;// = "/v1/news/categories/%d/news";
    private final String DETAILED_NEWS;// = "/v1/news/details";

    public RestDataSource(Context context) {
        this.context = context;
        // parameters moved to resources to handle different api addresses for different language
        SERVER_URL = context.getResources().getString(R.string.server_host);
        CATEGORIES = context.getResources().getString(R.string.categories_api);
        NEWS = context.getResources().getString(R.string.news_api);
        DETAILED_NEWS = context.getResources().getString(R.string.details_api);
    }

    /**
     * Do GET request to specific api with parameters
     * @param addr - address of api to request
     * @param params - parameters
     * @return respond of server
     */
    //do request to rest api:
    private String doRest(String addr, HashMap<String, String> params) {
        String res = "";
        try {
            Uri.Builder uri = Uri.parse(SERVER_URL).buildUpon()
                    .path(addr);
            if (params!=null)
                for (String key: params.keySet())
                    uri = uri.appendQueryParameter(key,params.get(key));

            String url = uri.build().toString();
            res = new RestHttpClient(context).execute(url).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Gets news categories
     * @return list of CategoryItems
     */
    @Override
    public List<CategoryItem> getCategories() {
        ArrayList<CategoryItem> categoryItems = new ArrayList<>();
        String respond = doRest(CATEGORIES, null);
        try {
            JSONArray news = (JSONArray) new JSONObject(respond).get("list");
            for (int i = 0; i < news.length(); i++) {
                JSONObject item = (JSONObject) news.get(i);
                categoryItems.add(new CategoryItem(
                        (Integer) item.get("id"),
                        (String) item.get("name")
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return categoryItems;
    }

    /**
     * Gets news by category and page
     * @param categoryId - category id
     * @param page - news page
     * @return list of NewsItems
     */
    @Override
    public List<NewsItem> getNewsByCategoryIdOnPage(final long categoryId, final int page) {
        ArrayList<NewsItem> newsItems = new ArrayList<>();
        HashMap<String, String> params = new HashMap<>();
        params.put("page", String.valueOf(page));
        String respond = doRest(String.format(Locale.getDefault(), NEWS, categoryId), params);
        try {
            JSONArray news = (JSONArray) new JSONObject(respond).get("list");
            for (int i = 0; i < news.length(); i++) {
                JSONObject item = (JSONObject) news.get(i);
                newsItems.add(new NewsItem(
                        (Integer) item.get("id"),
                        (String) item.get("title"),
                        (String) item.get("date"),
                        (String) item.get("shortDescription")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newsItems;
    }

    /**
     * Gets detailed info about news
     * @param id - id of news
     * @return FullNewsItem structure
     */
    @Override
    public FullNewsItem getDetailedNewsInfo(long id) {
        FullNewsItem fullNewsItems = null;
        HashMap<String, String> params = new HashMap<>();
        params.put("id", String.valueOf(id));
        String respond = doRest(DETAILED_NEWS, params);
        try {
            JSONObject fullNews = (JSONObject) new JSONObject(respond).get("news");
            fullNewsItems = new FullNewsItem(
                    (Integer) fullNews.get("id"),
                    (String) fullNews.get("title"),
                    (String) fullNews.get("date"),
                    (String) fullNews.get("shortDescription"),
                    (String) fullNews.get("fullDescription"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return fullNewsItems;
    }
}
