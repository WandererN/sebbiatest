package com.app.jh.testsebbia.ListImprovers;

import android.content.Context;
import android.widget.AbsListView;

import com.app.jh.testsebbia.DataSources.RestDataSource;
import com.app.jh.testsebbia.DataStructures.NewsItem;

import java.util.List;
import java.util.concurrent.CancellationException;

/**
 * Created by Странник on 18.03.2017.
 */

public class PaginationScroller implements AbsListView.OnScrollListener {
    private int visibleThreshold = 0;
    private int currentPage = 0;
    private long id;
    private boolean isEnd = false;
    private Context context;
    private int previousTotal = 0;
    private boolean loading = true;


    public PaginationScroller(int visibleThreshold, long id, Context context) {
        this.visibleThreshold = visibleThreshold;
        this.id = id;
        this.context = context;
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
                currentPage++;
            }
        }
        if (!isEnd && !loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            try {
                List<NewsItem> items = new RestDataSource(context).getNewsByCategoryIdOnPage(id, currentPage);
                isEnd = items.size() == 0;
                if (absListView.getAdapter() instanceof NewsItemListAdapter) {
                    NewsItemListAdapter adapter = (NewsItemListAdapter) absListView.getAdapter();
                    adapter.addList(items);
                }
                loading = true;

            } catch (CancellationException e) {
                e.printStackTrace();

            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }
}
