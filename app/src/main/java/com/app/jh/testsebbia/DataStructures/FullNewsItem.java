package com.app.jh.testsebbia.DataStructures;

/**
 * Created by Странник on 15.03.2017.
 */

public class FullNewsItem extends NewsItem {
    private String fullDescription;

    public String getFullDescription() {
        return fullDescription;
    }

    public FullNewsItem(int id, String title, String date, String shortDescription,
                        String fullDescription) {
        super(id, title, date, shortDescription);
        this.fullDescription = fullDescription;
    }
}
