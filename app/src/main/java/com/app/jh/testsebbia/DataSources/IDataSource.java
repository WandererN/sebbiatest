package com.app.jh.testsebbia.DataSources;

import com.app.jh.testsebbia.DataStructures.CategoryItem;
import com.app.jh.testsebbia.DataStructures.FullNewsItem;
import com.app.jh.testsebbia.DataStructures.NewsItem;

import java.util.List;

/**
 * Created by Странник on 14.03.2017.
 */

public interface IDataSource {
    List<CategoryItem> getCategories();

    List<NewsItem> getNewsByCategoryIdOnPage(long categoryId, int page);

    FullNewsItem getDetailedNewsInfo(long id);
}
