package com.app.jh.testsebbia.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

/**
 * Created by Странник on 15.03.2017.
 */

public class RestHttpClient extends AsyncTask<String, String, String> {
    private final static int READ_TIMEOUT_MILLIS = 3000;
    private final static int CONNECT_TIMEOUT_MILLIS = 3000;
    private final Context context;

    public RestHttpClient(Context context) {
        this.context = context;
    }

    private String doGet(String addr) throws IOException {
        String res = "";
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        URL url = null;

        try {
            url = new URL(addr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            if (url != null) {
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(READ_TIMEOUT_MILLIS);
                connection.setConnectTimeout(CONNECT_TIMEOUT_MILLIS);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Accept", "application/json");
                connection.setDoInput(true);
                connection.connect();
                int code = connection.getResponseCode();
                if (code != HttpURLConnection.HTTP_OK)
                    throw new IOException("HTTP error code: " + code);
                inputStream = connection.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int n;
                while ((n = inputStream.read()) != -1)
                    baos.write(n);
                res = new String(baos.toByteArray(), StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            cancel(true);
            e.printStackTrace();
        } finally {
            // Close stream and connection if exists.
            if (inputStream != null) {
                inputStream.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return res;
    }


    @Override
    protected String doInBackground(String... url) {
        if (isCancelled())
            return null;
        String res = "";
        try {
            res = doGet(url[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    protected void onPreExecute() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork == null) {
            cancel(true);
            return;
        }
        boolean isConnected = activeNetwork.isConnectedOrConnecting();
        if (!isConnected)
            cancel(true);
    }

}
