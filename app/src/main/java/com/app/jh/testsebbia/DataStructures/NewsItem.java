package com.app.jh.testsebbia.DataStructures;

/**
 * Created by Странник on 15.03.2017.
 */
public class NewsItem {
    public NewsItem(long id, String title, String date, String shortDescription) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.shortDescription = shortDescription;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    private long id;
    private String title;
    private String date;
    private String shortDescription;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsItem newsItem = (NewsItem) o;

        if (id != newsItem.id) return false;
        if (title != null ? !title.equals(newsItem.title) : newsItem.title != null) return false;
        if (date != null ? !date.equals(newsItem.date) : newsItem.date != null) return false;
        return shortDescription != null ? shortDescription.equals(newsItem.shortDescription) : newsItem.shortDescription == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (shortDescription != null ? shortDescription.hashCode() : 0);
        return result;
    }
}
