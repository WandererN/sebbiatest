package com.app.jh.testsebbia.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.app.jh.testsebbia.DataSources.IDataSource;
import com.app.jh.testsebbia.DataSources.RestDataSource;
import com.app.jh.testsebbia.DataStructures.NewsItem;
import com.app.jh.testsebbia.ListImprovers.NewsItemListAdapter;
import com.app.jh.testsebbia.ListImprovers.PaginationScroller;
import com.app.jh.testsebbia.R;

import java.util.concurrent.CancellationException;

public class NewsInCategoryActivity extends AppCompatActivity {
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_in_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.idNewsToolbar);
        setSupportActionBar(toolbar);
        intent = getIntent();
        String categoryName = intent.getStringExtra("categoryName");

        getSupportActionBar().setTitle(R.string.app_name);
        getSupportActionBar().setSubtitle(categoryName);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAndFillListView();
            }
        });
        createAndFillListView();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void createAndFillListView() {
        try {
            final Intent detailedNewsActivityIntent = new Intent(this, DetailedNewsActivity.class);
            ListView newsListView = (ListView) findViewById(R.id.idNewsListView);
            IDataSource dataSource = new RestDataSource(getApplicationContext());
            int categoryId = intent.getIntExtra("categoryId", 0);
            final ListAdapter newsListAdapter = new NewsItemListAdapter(getLayoutInflater(), dataSource.getNewsByCategoryIdOnPage(categoryId, 0));
            newsListView.setAdapter(newsListAdapter);
            newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    NewsItem selected = (NewsItem) adapterView.getAdapter().getItem(i);
                    detailedNewsActivityIntent.putExtra("newsId", selected.getId());
                    detailedNewsActivityIntent.putExtra("data", selected.getDate());
                    detailedNewsActivityIntent.putExtra("title", selected.getTitle());
                    detailedNewsActivityIntent.putExtra("shortDescr", selected.getShortDescription());
                    detailedNewsActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(detailedNewsActivityIntent);
                }
            });
            newsListView.setOnScrollListener(new PaginationScroller(1, categoryId, getApplicationContext()));
            Snackbar.make(findViewById(R.id.content_news_in_category), getString(R.string.news_loaded), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } catch (CancellationException e) {
            Snackbar.make(findViewById(R.id.content_news_in_category), getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
