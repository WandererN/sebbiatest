package com.app.jh.testsebbia.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.app.jh.testsebbia.DataSources.IDataSource;
import com.app.jh.testsebbia.DataSources.RestDataSource;
import com.app.jh.testsebbia.DataStructures.CategoryItem;
import com.app.jh.testsebbia.R;

import java.util.List;
import java.util.concurrent.CancellationException;

public class SebbiaTestMainActivity extends AppCompatActivity {
    ListView newsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sebbia_test_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.idCategoriesToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        getSupportActionBar().setSubtitle(R.string.categories);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getSupportActionBar().setLogo(getDrawable(R.mipmap.ic_launcher));
        } else {
            getSupportActionBar().setLogo(getResources().getDrawable(R.mipmap.ic_launcher));
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAndFillListView();
            }
        });
        createAndFillListView();
    }

    private void createAndFillListView() {
        newsListView = (ListView) findViewById(R.id.idNewsCategoriesListView);
        List<CategoryItem> categoryItemList;
        final IDataSource dataSource = new RestDataSource(getApplicationContext());
        try {
            categoryItemList = dataSource.getCategories();
            final ArrayAdapter<CategoryItem> categoryItemArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, categoryItemList);
            final Intent intent = new Intent(this, NewsInCategoryActivity.class);
            newsListView.setAdapter(categoryItemArrayAdapter);
            newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    intent.putExtra("categoryId", i);
                    intent.putExtra("categoryName", newsListView.getAdapter().getItem(i).toString());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });
            Snackbar.make(findViewById(R.id.content_sebbia_test_main), getString(R.string.categories_loaded), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } catch (CancellationException e) {
            Snackbar.make(findViewById(R.id.content_sebbia_test_main), getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            e.printStackTrace();
        }
    }

}
